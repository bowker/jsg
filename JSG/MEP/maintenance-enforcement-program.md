# Maintenance enforcement program

<p class="lede">The Maintenance Enforcement Program (MEP) collects and enforces court-ordered child support, spousal support and partner support.</p>

**Pages in this section**

[Register with MEP](future.link)

[Access your MEP account](future.link)

[MEP case officers](future.link)

[Do you get child, partner or spousal support](future.link)

[Do you pay child, partner or spousal support](future.link)

[Child support recalculation program](future.link)

[Support payments outside Alberta](future.link)

[Employers’ responsibilities](future.link)

[Make court orders enforceable](future.link)

[Help find people who owe support](future.link)

[MEP forms](future.link)

[Contact MEP](future.link)

## Are you eligible for MEP

You can [register with MEP](future.link) if all 3 conditions apply:

 1. You or the other party live in Alberta.
 2. You pay or receive child, spousal or partner support.
 3. You have a court order for support – a maintenance order or a maintenance agreement – filed with the court.
## What MEP does

MEP enforces court-ordered child support, spousal support or partner support payments for its clients.
### MEP is a ‘go-between’ for clients
You have to register with MEP and become a client. Afterwards, we will enforce a support agreement by working with these parties:
 - payor of support – the person making the payments
 - recipient of support – the person getting the payments
#### MEP collects and forwards payments
MEP receives the payor’s support payments and then deposits them in the recipient’s bank account.

If the recipient gets government income support, the government gets the support payments.

It can take up to 10 days to release the money because:
 - we use a trust account
 - the payment has to clear before we can put it in the recipient’s bank account
> MEP does its best to collect the support payments for each recipient. However, we can’t guarantee every payor will make the support payments.
### MEP has legal authority
MEP has the authority to enforce:

 - maintenance orders granted in Canada
 - maintenance orders granted in other countries where Alberta has agreements
 - specific types of support agreements such as:
	 - paternity agreements under the _Parentage and Maintenance Act
	 - agreements made under the _Income and Employment Support Act_ and the _Child Youth and Family Enhancement Act_
- maintenance enforcement support agreements – also used under the _Family Law Act_ – if they:
	- meet the Maintenance Enforcement Regulation
	- are filed with the Court of Queen’s Bench
- recalculation decisions from:
	- the [Child Support Recalculation Program](https://justice.alberta.ca/programs_services/families/recalculation/Pages/default.aspx)
	- any other place that has an agreement with MEP
#### MEP can recover unpaid support

MEP has legal tools to recover unpaid support payments. We can legally:
 - use databases to find a:
	 - payor of support
	 - payor’s assets
- collect a payor’s money from:
	- employers
	- banks
	- the federal government
- restrict a payor’s:
	- driver’s licence
	- passport

To learn more, read about [paying child, spousal or partner support](future.link).
### MEP works with you
Either party – the payor or recipient of support – may [submit a registration package to MEP](future.link) to enforce their file.

When we open a file:
- the other party is registered automatically
- a [MEP case officer](future.link) is assigned to each client
### MEP client privacy

MEP commits to keeping your personal information secure and confidential. We collect personal information:
- as set out in the _Maintenance Enforcement Act_ and the Maintenance Enforcement Regulation
- to enforce maintenance orders and agreements

> Contact MEP immediately if:
> - you receive information meant for someone else
> - you are a MEP client and you believe your privacy has been breached
### Authorize a representative for MEP
You can have someone be your authorized representative. Your representative can:
- get your file information from us
- give us information about you
- deal with us for things like:
	- making payment arrangements
	- asking for a [Child Status Review](future.link)
#### Authorize your representative for MEP
1. Fill out the [Client Authorization of Third Party](https://cfr.forms.gov.ab.ca/form/mep11269.pdf) form.
2. Mail, fax or email the completed form to MEP:

	Maintenance Enforcement Program
	7th Floor, John E. Brownlee Building
	10365 97 Street NW
	Edmonton, Alberta  T5J 3W7

	Fax: 780-401-7575

	Email: [albertamep@gov.ab.ca](mailto:albertamep@gov.ab.ca)
## Child access and support payments
### Access and support are separate issues
- MEP requires child support to be paid – even if support payors aren’t getting access to their children
- support recipients should obey their court orders for access – even if child support isn’t being paid
### Find help to enforce parenting time
#### If you are not getting to see your children
Read how to [enforce parenting time](https://www.alberta.ca/enforce-parenting-time.aspx).
#### If you need help with a legal dispute
Visit [Resolution and Court Administration Services (RCAS)](https://www.alberta.ca/rcas.aspx).
## Contact
Phone: [780-422-5555](tel:+17804225555)
Toll-free in Alberta: 310-0000 then 780-422-5555  
Fax: 780-401-7575
Email: [albertamep@gov.ab.ca](mailto:albertamep@gov.ab.ca)
  
Hours: 8:15 am - 12:00 pm, 1:00 pm - 4:30 pm (Monday to Friday, closed statutory holidays)

Maintenance Enforcement Program
7th Floor, John E. Brownlee Building
10365 97 Street NW
Edmonton, Alberta  T5J 3W7

[Open in Google maps](https://goo.gl/maps/h9M3LFeSkMH2).

_or link to a [MEP Contact page](future.link) if we want one_
<!-- end of user-space content -->
<hr>

## Sources and page information - not for production
**Ministry:** Justice and Solicitor General
**Contact email:** [trevor.black@gov.ab.ca](mailto:trevor.black@gov.ab.ca)
**Source URL(s)**: [https://www.justice.alberta.ca/programs_services/mep/Pages/default.aspx](https://www.justice.alberta.ca/programs_services/mep/Pages/default.aspx)
[https://www.justice.alberta.ca/programs_services/mep/pages/eligible.aspx](https://www.justice.alberta.ca/programs_services/mep/pages/eligible.aspx)
[https://www.justice.alberta.ca/programs_services/mep/InfoSheets/WelcomeMEP.pdf](https://www.justice.alberta.ca/programs_services/mep/InfoSheets/WelcomeMEP.pdf)
[https://www.justice.alberta.ca/programs_services/mep/InfoSheets/ResponsibilitiesMEP.pdf](https://www.justice.alberta.ca/programs_services/mep/InfoSheets/ResponsibilitiesMEP.pdf)
[https://www.justice.alberta.ca/programs_services/mep/Pages/FirstTimeAccountAccess.aspx](https://www.justice.alberta.ca/programs_services/mep/Pages/FirstTimeAccountAccess.aspx)
[https://www.justice.alberta.ca/programs_services/mep/InfoSheets/MEPAccountsOnlineMEPInfoLine.pdf](https://www.justice.alberta.ca/programs_services/mep/InfoSheets/MEPAccountsOnlineMEPInfoLine.pdf)
[https://www.justice.alberta.ca/programs_services/mep/InfoSheets/ClientConfidentiality.pdf](https://www.justice.alberta.ca/programs_services/mep/InfoSheets/ClientConfidentiality.pdf)
[https://www.justice.alberta.ca/programs_services/mep/Pages/contact.aspx?WT.svl=sideNav](https://www.justice.alberta.ca/programs_services/mep/Pages/contact.aspx?WT.svl=sideNav)
**Stage URL:** [https://stage.alberta.ca/new-url.aspx](https://stage.alberta.ca/new-url.aspx)
**RedDot ID:** TBD
**Proposed breadcrumb**: Home / Life events / Divorce and separation
**Markup Instructions:** Build new page
**Page Type:** program landing





<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEzOTkwNDIwMzldfQ==
-->