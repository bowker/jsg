# Register with Maintenance Enforcement Program

<p class="lede">Register with the Maintenance Enforcement Program (MEP) to have us collect your court-ordered child support, spousal support or partner support for you.</p>

## Before you register
Make sure you're eligible and know what you need to register for MEP .
### Are you eligible

You can [register with MEP](future.link) if all 3 conditions apply:

 1. You or the other party live in Alberta.
 2. You pay or receive child, spousal or partner support.
 3. You have a court order for support – a maintenance order or a maintenance agreement – filed with the court.
### Have a court order
You need a court order for support. A court order for support is either:
- a maintenance order or 
- a maintenance agreement

A maintenance order sets an enforceable amount of support that is payable for a child, spouse or adult interdependent partner.

A maintenance agreement creates an enforceable support agreement for a child, spouse or adult interdependent partner.
#### Get a maintenance order
It is a good idea to have a lawyer help with the maintenance order. If you do not have a lawyer, get help from [Resolution and Court Administration Services](https://www.alberta.ca/rcas.aspx).
#### Make and file a maintenance agreement
Be sure you are allowed to make a maintenance agreement. You cannot make a maintenance agreement if you:
- already have a maintenance order
- have an agreement under the:
	- _Parentage and Maintenance Act_
	- _Income Support Recovery Act_
	- _Income and Employment Supports Act_ (Part 5)
- get:
	- Assured Income for the Severely Handicapped (AISH)
	- Income Support benefits
	- other benefits from the Ministry of Community and Social Services

If you can make a maintenance agreement, it is a good idea to have a lawyer help with the maintenance order. If you do not have a lawyer, get help from [Resolution and Court Administration Services](https://www.alberta.ca/rcas.aspx).


## How to register

## After you register

## Contact

Phone: [780-422-5555](tel:+17804225555)
Toll-free in Alberta: 310-0000 then 780-422-5555
Fax: 780-401-7575
Email: [albertamep@gov.ab.ca](mailto:albertamep@gov.ab.ca)
  
Hours: 8:15 am - 12:00 pm, 1:00 pm - 4:30 pm (Monday to Friday, closed statutory holidays)

Maintenance Enforcement Program
7th Floor, John E. Brownlee Building
10365 97 Street NW
Edmonton, Alberta  T5J 3W7

[Open in Google maps](https://goo.gl/maps/h9M3LFeSkMH2).

_or link to a [MEP Contact page](future.link) if we want one_
<!-- end of user-space content -->
<hr>

## Sources and page information - not for production
**Ministry:** Justice and Solicitor General
**Contact email:** [trevor.black@gov.ab.ca](mailto:trevor.black@gov.ab.ca)
**Source URL(s)**: [https://www.justice.alberta.ca/programs_services/mep/Pages/how-to-register.aspx](https://www.justice.alberta.ca/programs_services/mep/Pages/how-to-register.aspx)
[https://www.justice.alberta.ca/programs_services/mep/InfoSheets/MaintenanceEnforcementSupportAgreements.pdf](https://www.justice.alberta.ca/programs_services/mep/InfoSheets/MaintenanceEnforcementSupportAgreements.pdf)
**Stage URL:** [https://stage.alberta.ca/new-url.aspx](https://stage.alberta.ca/new-url.aspx)
**RedDot ID:** TBD
**Proposed breadcrumb**: Home / Life events / Divorce and separation
**Markup Instructions:** Build new page
**Page Type:** content
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTYwNjA2NjA0OV19
-->