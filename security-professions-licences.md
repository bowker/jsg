# Security licences and permits

<p class="lede">Apply for licences for security, locksmith and investigative professionals and businesses, and body armour permits.</p>

__Pages in this section:__

	Automotive lock bypass worker licence
	Body armour permit
	Investigator licence
	Locksmith equipment sales worker licence
	Locksmith licence
	Security licences from participating registries
	Security service worker licence
	Security, investigation and locksmith business resources

## Overview

Security and investigative services is a growing and important job sector in Alberta. This sector helps promote safe communities and includes occupations like:

- security service worker
- locksmith
- investigator
- automotive lock bypass worker
- locksmith equipment sales worker

Typical users of security and investigative services include:

- malls
- event centres
- public transportation
- businesses
- banks
- individuals

The Security Programs department is responsible for the oversight, licensing and compliance of security, investigative and locksmith:

- professionals
- businesses

### Security licences from registries

[Participating registries](https://www.alberta.ca/security-licences-registries.aspx) can handle applications and renewals for security guards, locksmiths, investigators and more.
### Body armour permits

The Security Programs department issues permits to buy, transport or wear body armour.
### Contact

Hours: 8:15 am - 4:30 pm (Monday to Friday, closed statutory holidays)
Phone: [1-877-462-0791](tel:+18774620791)
Email: ssia.registrar@gov.ab.ca

Security Programs
P.O. Box 1023 Station Main
Edmonton, Alberta  T5J 2M1
## Mail service disruption

Special instructions and information about licence applications and training certificates during a mail-service disruption.

If you have any questions about your options:
- call [1-877-462-0791](tel:+18774620791)
- email [ssia.registrar@gov.ab.ca](mailto:ssia.registrar@gov.ab.ca)

You will receive a response within one business day.

### Training certificates

Certificates will not be mailed or emailed during this time.

Contact your training provider for assistance.

### Individual applications

> Temporary licences valid for 60 days are issued while individual
> licences are processed.

These are your options to apply for an individual licence:

#### Participating registry agents

Qualified individuals are encouraged to apply at [participating registry offices](https://www.alberta.ca/security-licences-registries.aspx). Registry agents charge a service fee in addition to the licensing fee.

#### Calgary drop off

Applications must be sealed in an envelope labeled with the address below and slid under the office door.  Staff are not available at this location. If you need help, call [1-877-462-0791](tel:+18774620791).

Security Programs
Law Enforcement and Oversight Branch
3rd Floor, John J. Bowlen Building
620 7 Avenue SW, Suite 300
Calgary, Alberta

[Open in Google maps](https://goo.gl/maps/f9RTahEKiBD2)

#### Edmonton drop off

Applications must be sealed in an envelope labeled with the address below and left in the drop-box at reception. Staff are not available at this location. If you need help, call [1-877-462-0791](tel:+18774620791).

Security Programs
Law Enforcement and Oversight Branch
9th Floor, John E. Brownlee Building
10365 97 Street NW
Edmonton, Alberta

[Open in Google maps](https://goo.gl/maps/f5xvamzf4152)

#### Courier

Have your application delivered to:

Security Programs
Alberta Justice and Solicitor General
9th Floor, John E. Brownlee Building
10365 97 Street NW
Edmonton, Alberta  T5J 3W7

### Business applications

Submit your business application by courier or drop-off:

Security Programs
Alberta Justice and Solicitor General
9th Floor, John E. Brownlee Building
10365 97 Street NW
Edmonton, Alberta  T5J 3W7
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTkyODE3NjE0NCwtMzkzMTQ0NDM3LDU4Mj
M3NzExM119
-->